package top.jfunc.websocket;

import top.jfunc.websocket.memory.MemWebSocketManager;

/**
 * 使用入口类，使用此类来获取到 WebSocketManager 使用 WebSocket 功能
 * @author xiongshiyan at 2019/4/20 , contact me with email yanshixiong@126.com or phone 15208384257
 */
public class WebSocketManagerUtil {
    private WebSocketManagerUtil(){}

    /**
     * 默认具备基于内存的
     */
    private static WebSocketManager webSocketManager = new MemWebSocketManager();

    public static WebSocketManager getWebSocketManager() {
        return webSocketManager;
    }

    /**
     * 初始化的时候调用此方法
     */
    public static void setWebSocketManager(WebSocketManager webSocketManager) {
        WebSocketManagerUtil.webSocketManager = webSocketManager;
    }
}
