package top.jfunc.websocket.redis.action;

/**
 * @author xiongshiyan at 2019/4/20 , contact me with email yanshixiong@126.com or phone 15208384257
 */
public class ActionFactory {
    public static Action getAction(String actionName){
        switch (actionName){
            /**
             * @see SendMessageAction#NAME
             * @see SendMessageAction#INSTANCE
             */
            case "top.jfunc.websocket.redis.action.SendMessageAction": return SendMessageAction.INSTANCE;
            case "top.jfunc.websocket.redis.action.BroadCastAction": return BroadCastAction.INSTANCE;
            case "top.jfunc.websocket.redis.action.RemoveAction": return RemoveAction.INSTANCE;
            default:return NoActionAction.INSTANCE;
        }
    }

    public static void main(String[] args) {
        System.out.println(SendMessageAction.NAME);
        Action action = ActionFactory.getAction(SendMessageAction.NAME);
        System.out.println(action);
    }
}

